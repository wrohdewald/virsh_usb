# virsh_usb
Facilitate permanently attaching USB devices to libvirt guests:

- works even with identical USB vendor/product ids
- survives rebooting or removing/inserting the USB device

# Usage

1. copy etc/udev/rules.d/* to /etc/udev/rules.d
1. rename/edit same
1. put bin/* into something like /usr/local/bin
1. udevadm control --reload
1. make sure that the consumers in the virtual guest reconnect automatically

My use case is running Home Assistant in a virtual guest. It uses two USB sticks,
one for Z-Wave and one for Zigbee. Both have the same vendor/product id, but luckily
they have different serial numbers.

Now I can do whater I want, Home Assistant keeps working, like

- reboot host
- reboot guest
- remove and re-insert USB stick at any time
- chase the cat


# Caveat:

The scripts called by udev are required to return very quickly. If you encounter
timing problems, maybe virsh attach-device should run in background.
If you need to that, please tell me about it!


# For details also see

    virsh_xml_for --help
    bind_usb_to_guest --help

# Support

If you have a question, I should improve the documentation.

So, please open an issue for every question at https://gitlab.com/wrohdewald/virsh_usb

# License

GPL-2.0
